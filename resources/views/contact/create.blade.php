@extends('contact')

@section('main')
    <div class="row">
        <div class="col-md-8 offset-sm-2">
            <h2 class="display-6">Nouveau Contact</h2>
        </div>
    </div>

    <div class="row">
        
        <div class="col-md-8 offset-sm-2">
        <form action="{{url("/contacts")}}" method="post">
            @csrf
            <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" type="text" name="name">

                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" type="text" name="email">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input class="form-control" type="text" name="phone">
                </div>
                <div class="form-group">
                    <label for="Address">Adresse</label>
                    <textarea class="form-control"  name="address"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </form>

        </div>
    </div>    
@endsection