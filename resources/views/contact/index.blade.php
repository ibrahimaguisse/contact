@extends('contact')

@section('main')
<a href="{{url("contacts/create")}}" class="btn btn-success mb-2">Nouveau contact</a>
@csrf
<input type="hidden" name="_method" value="PUT">
    <div class="row">
        <div class="col-md-12">
            <table class="table table stripped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th colspan="2">Actions</th>
                    </tr>    
                </thead>
                <tbody>
                    @foreach ($contacts as $contact)
                    <tr>
                        <td>{{$contact->id}}</td>
                        <td>{{$contact->name}}</td>
                        <td>{{$contact->email}}</td>
                        <td>{{$contact->phone}}</td>
                        <td>{{$contact->address}}</td>  
                    <td><a  class="btn btn-primary" href="url{{"contact/{$contact->id}/edit"}}" >Modifier
                    </a>
                    </td>  
                        <td>
                        <form action="{{url("contact/{$contact->id}")}}" method="post">
                            @method('DELETE')
                              @csrf
                             
                              <button class="btn btn-danger">Supprimer</button>

                        </form>  
                        </td>  

                    </tr>  
                    @endforeach  
                </tbody>    
            </table>    
        </div>    
    </div>   
@endsection