<?php
namespace App\Repositories;
use App\Contact;
use App\Http\Controllers\ContactController;

interface ContactsRepositoryInterface{
   public function save(Contact $contact);
}
