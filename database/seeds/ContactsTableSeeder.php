<?php

use App\Contact;
use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Contact::class, 50)->create()->each(function ($contact) {
            $contact->save();
            });
        Contact::table('contacts')->insert(
            array(
            array(
            'name' => 'ibrahima',
            'email' => 'iguisse97@gmail.com',
            'phone' => '0605674147',
            'address' => 'ramonville',
            ),
            array(
                'name' => 'ibrahima',
                'email' => 'iguisse97@gmail.com',
                'phone' => '0605674147',
                'address' => 'ramonville',
            ),
            )
            );
            }
           
}
